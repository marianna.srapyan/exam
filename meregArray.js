function mergeArray( arr1, arr2 ) {
    let newArray = [];
    let index1 = 0;
    let index2 = 0;
    let fullLength = arr1.length + arr2.length;
    while(newArray.length < fullLength) {
        if (!arr2[index2] && arr1[index1]) {
            newArray.push(arr1[index1]);
            index1++;
            continue;
        }
        if (arr2[index2] && !arr1[index1]) {
            newArray.push(arr2[index2]);
            index2++;
            continue;
        }
        if (!arr1[index1] && !arr2[index2]) {
            newArray.push(arr2[index2]);
            index2++;
            continue;

        }
        if (arr1[index1] > arr2[index2]) {
            newArray.push(arr2[index2]);
            index2++;
            continue;

        }
        if (arr1[index1] < arr2[index2]) {
            newArray.push(arr1[index1]);
            index1++;
        }
    }
    console.log(newArray);
    return newArray;
}
/* unit test*/
function randomArray() {
    let arrayLength = Math.floor(Math.random() * 10);
    arrayLength === 0 ? arrayLength = 5 : arrayLength = arrayLength;
    let array = [];
    for (let i = 0; i < arrayLength; i++) {
        array[i] = Number(Math.floor(Math.random() * 100));
    }
    return array.sort();
}

mergeArray(randomArray(), randomArray());
