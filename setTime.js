function setTime () {
    let delay = 1000;
    let num = 1;
    let timerId = setTimeout(function printNum() {
        console.log(num);
        num++
        delay = num * 1000;
        timerId = setTimeout(printNum, delay);
        if (delay > 30000) {
            clearInterval(timerId);
        }
    }, delay);
}
setTime()
