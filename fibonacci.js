function fibonacci(n) {
    let cache = {};
    let fib1;
    let fib2;
    if (n === 0 || n === 1 ) {
        return n
    }
    if (n-1 in cache) {
        fib1 = cache[n-1];
    } else {
        cache[n-1] = fibonacci(n-1);
        fib1 = cache[n-1];
    }
    if (n-2 in cache) {
        fib2 = cache[n-2];
    } else {
        cache[n-2] = fibonacci(n-2);
        fib2 = cache[n-2];
    }
    return fib1 + fib2;

}
