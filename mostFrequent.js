function itemTimes(arr) {
    let obj = {};
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] in obj) {
            obj[arr[i]] = obj[arr[i]] + 1
            continue;
        }
        obj[arr[i]] = 1;
    }
    const key = Object.keys(obj).reduce((a, b) => obj[a] > obj[b] ? a : b);
    return `${key}(${obj[key]} times)`;
}
